package gr.unipi.ds.msc.utils;

import java.io.Serializable;

import org.apache.spark.AccumulableParam;

 /**
  * Class to implement LongAccumulator (Accumulable param of Long type)
  */

public class LongAccumulator implements AccumulableParam<Long, Long>, Serializable {

	private static final long serialVersionUID = -7442834308468589305L;

	/**
	 * Adds a Long value to an accumulator
	 * @param r accumulator
	 * @param t value to be added
	 */
	public Long addAccumulator(final Long r, final Long t) {
		return r + t;
	}

	/**
	 * Adds two Long accumulators
	 * @param r1 Long accumulator A
	 * @param r2 Long accumulator B
	 */
	public Long addInPlace(final Long r1, final Long r2) {
		return r1 + r2;
	}

	/**
	 * Sets long accumulator's initial value
	 * @param initialValue Long accumulator's initialValue
	 */
	public Long zero(final Long initialValue) {
		return 0L;
	}

}
