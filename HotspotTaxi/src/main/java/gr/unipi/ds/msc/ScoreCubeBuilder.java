package gr.unipi.ds.msc;

import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.broadcast.Broadcast;

import gr.unipi.ds.msc.utils.Cell;
import gr.unipi.ds.msc.utils.Params;
import gr.unipi.ds.msc.utils.Statistics;
import gr.unipi.ds.msc.utils.TupleComparator;
import scala.Tuple2;

public class ScoreCubeBuilder {
	
	/**
	 * Calculates the zScore (by the G*i statistic formula) of a Cell
	 * @param c The cell against which the zScore will be calculated
	 * @param population The population (i.e. passenger count) of the cell and its neighbors
	 * @param params A Params object representing the input command line arguments
	 * @param statistics A Statistics object representing the calculated statistics (i.e. mean value, standard deviation, e.t.c)
	 * @return The calculated zScore by using the G*i statistic formula
	 */
	public static Tuple2<Double, String> getCellScore(Cell c, int population, Params params, Statistics statistics) {
		//get the count of neighbors that fall in the bounding box
		int neighborCount = c.getCountOfNeighbors(params);
		// calculate and return zScore (Gi statistic formula)
		double numerator = (double) (population - statistics.passengerMean * neighborCount);
		double denominator = (double) (statistics.standardDeviation * Math.sqrt((statistics.n * neighborCount - Math.pow(neighborCount, 2)) / ((double) statistics.n - 1)));

		return new Tuple2<Double, String>(numerator / denominator,
				(int) (-c.getX() + params.maxX) + "," + (int) (c.getY() + params.minY) + "," + c.getZ());
	}
	
	/**
	 * Checks the newTuple against the queue, to determine whether newTuple is among the top-50 Cells according to their zScores
	 * @param queue The top-50 queue
	 * @param newTuple The new tuple to be checked
	 */
	private static void checkAndAddToQueue(PriorityQueue<Tuple2<Double, String>> queue, Tuple2<Double, String> newTuple) {
		if (queue.size() < 50) {
			//queue is not filled up yet...
			queue.add(newTuple);
		}
		else {
			//queue is filled up, and newTuple should be part of the queue if newTuple's score is greater than the lowest score in queue
			if (queue.peek()._1 < newTuple._1) {
				queue.poll();  //delete the lowest score, to make place for the newTuple
				queue.add(newTuple);
			}
		}
	}
	
	/**
	 * Transforms a neighbor cube to a score cube, where each cell is represented by its zScore
	 * @param neighborCube The neighbor cube to be transformed
	 * @param broadcastParams A Params object that has been broadcasted to nodes in the cluster
	 * @param broadcastStatistics A Statistics object that has been broadcasted to nodes in the cluster
	 * @return
	 */
	public static JavaPairRDD<Double, String> generateScoreCubeLongKey(JavaPairRDD<Long, Integer> neighborCube,
			final Broadcast<Params> broadcastParams, final Broadcast<Statistics> broadcastStatistics) {
		return neighborCube
				.mapPartitionsToPair(new PairFlatMapFunction<Iterator<Tuple2<Long, Integer>>, Double, String>() {
					private static final long serialVersionUID = 6538948665471333479L;

					public Iterable<Tuple2<Double, String>> call(Iterator<Tuple2<Long, Integer>> iter) throws Exception {
						//Initialize a comparator that will keep elements in queue sorted according to their zScores descending
						Comparator<Tuple2<Double, String>> tupleComparator = new TupleComparator();
						//Initialize a queue responsible for storing the top-50 cells according to their zScores
						PriorityQueue<Tuple2<Double, String>> queue = new PriorityQueue<Tuple2<Double, String>>(50, tupleComparator);
						while (iter.hasNext()) {
							Tuple2<Long, Integer> t = iter.next();
							//Transform the long key to an actual Cell object, with x, y, z values
							Cell c = new Cell(t._1, broadcastParams.value());
							//Calculate the zScore of the cell
							Tuple2<Double, String> newTuple = getCellScore(c, t._2, broadcastParams.value(), broadcastStatistics.value());
							//Check and update the queue by considering newTuple object
							checkAndAddToQueue(queue, newTuple);
						}
						//Return only the top-50 calculated cells
						return queue;
					}
				}).cache();
	}
}
