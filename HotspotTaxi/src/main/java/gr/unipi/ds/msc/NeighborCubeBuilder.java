package gr.unipi.ds.msc;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.broadcast.Broadcast;

import gr.unipi.ds.msc.utils.Cell;
import gr.unipi.ds.msc.utils.Params;
import scala.Tuple2;

public class NeighborCubeBuilder {
	
	/**
	 * Given a specific Cell object in cube, creates a list that presents the cell object to its neighbors
	 * @param c The cell object
	 * @param population The population (i.e. the passenger count) of the cell object
	 * @param params A Params object representing the input command line arguments
	 * @return A list that presents the cell object to its neighbors
	 */
	private static List<Tuple2<Long, Integer>> getNeighborList(Cell c, int population, Params params) {
		List<Tuple2<Long, Integer>> result = new ArrayList<Tuple2<Long, Integer>>();
		int x = (int) c.getX();  //longitude id of cell
		int y = (int) c.getY();  //latitude id of cell
		int z = (int) c.getZ();  //datetime id of cell
		
		//For the individual x, y, z cordinates check if the neighbor cell falls in the bounding box
		for (long i = x - 1; i <= x + 1; i++) {
			if ((i <= 0) || (i > params.longitudeSegments))
				continue;
			for (int j = y - 1; j <= y + 1; j++) {
				if ((j < 0) || (j >= params.latitudeSegments))
					continue;
				for (int k = z - 1; k <= z + 1; k++) {
					if ((k < 0) || (k >= params.dateSegments))
						continue;
					result.add(new Tuple2<Long, Integer>(Cell.getId(i, j, k, params), population));
				}
			}
		}
		return result;
	}
	
	/**
	 * Transforms the original cube RDD to a neighbor cube RDD, where each cell presents its informations to its neighbors
	 * @param firstGrid The original cube RDD
	 * @param broadcastParams A Params object that has been broadcasted to nodes in the cluster
	 * @return A raw neighbor cube RDD that it is to be aggregated to a neighbor cube RDD
	 */
	public static JavaPairRDD<Long, Integer> generateNeighborLongKey(JavaPairRDD<Long, Integer> firstGrid, final Broadcast<Params> broadcastParams) {
		return firstGrid.flatMapToPair(new PairFlatMapFunction<Tuple2<Long, Integer>, Long, Integer>() {
			private static final long serialVersionUID = 5550684576446467838L;

			public Iterable<Tuple2<Long, Integer>> call(Tuple2<Long, Integer> t) throws Exception {
				long id = t._1;
				Cell c = new Cell(id, broadcastParams.value());
				List<Tuple2<Long, Integer>> list = getNeighborList(c, t._2, broadcastParams.value());
				return list;
			}
		});
	}
	
	/**
	 * Helper method that returns the sum of two arguments
	 */
	private static Function2<Integer, Integer, Integer> aggregateNeighborGrid = new Function2<Integer, Integer, Integer>() {
		private static final long serialVersionUID = 7906560560232105882L;

		public Integer call(Integer v1, Integer v2) throws Exception {
			return v1 + v2;
		}
	};
	
	/**
	 * Aggregates the raw Cell objects according to their ids and sums their individual populations
	 * @param rawGrid the RDD of the raw Cell objects
	 * @return a new RDD with aggregated Cell objects
	 */
	public static JavaPairRDD<Long, Integer> reduceLong(JavaPairRDD<Long, Integer> rawGrid) {
		return rawGrid.reduceByKey(aggregateNeighborGrid).cache();
	}
}
