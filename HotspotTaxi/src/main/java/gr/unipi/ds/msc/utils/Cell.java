
package gr.unipi.ds.msc.utils;


import org.apache.commons.lang.builder.HashCodeBuilder;
import scala.Serializable;

/**
 * Cell class to store x,y,z coordinates of each cell in the grid
 */
public class Cell implements Serializable {
	private static final long serialVersionUID = -7979234067700517896L;
	private long x;						//Longitude coordinate
	private long y;						//Latitude coordinate
	private long z;						//Timestep
	private transient int population;	//The population (i.e. passenger count) of the cell and its neighbors

	/**
	 * First Constructor of Cell Class
	 * @param x Longitude coordinate
	 * @param y Latitude coordinate
	 * @param z Timestep
	 * @param population The population (i.e. passenger count) of the cell and its neighbors
	 */
	public Cell(long x, long y, long z, int population) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.population = population;
	}
	
	/**
	 * Second Constructor of Cell Class
	 * @param id Long key used to identify Cell
	 * @param params A Params object that has been broadcasted to nodes in the cluster
	 */
	public Cell(long id, Params params) {
		z = (int) (id / (params.latitudeSegments * params.longitudeSegments));
		id -= z * params.latitudeSegments * params.longitudeSegments;
		y = (int) (id / params.longitudeSegments);
		x = (int) id - y * params.longitudeSegments;
	}
	
	/**
	 * Counts neighboring cells
	 * @param params A Params object that has been broadcasted to nodes in the cluster
	 * @return number of neighboring cells
	 */
	public int getCountOfNeighbors(Params params) {
		int number = 0;
		for (long i = x - 1; i <= x + 1; i++) {
			if ((i <= 0) || (i > params.longitudeSegments))
				continue;
			for (long j = y - 1; j <= y + 1; j++) {
				if ((j < 0) || (j >= params.latitudeSegments))
					continue;
				for (long k = z - 1; k <= z + 1; k++) {
					if ((k < 0) || (k >= params.dateSegments))
						continue;
					number++;
				}
			}
		}
		return number;
	}
	
	/**
	 * Transforms x,y,z coordinates into cell's id (Long key)
	 * @param x2 Longitude coordinate
	 * @param y2 Latitude coordinate
	 * @param z2 Timestep
	 * @param params A Params object that has been broadcasted to nodes in the cluster
	 * @return Cell's id
	 */
	public static long getId(long x2, long y2, long z2, Params params) {
		return (x2 + y2 * params.longitudeSegments + z2 * params.longitudeSegments * params.latitudeSegments);
	}
	
	/**
	 * Calls getId method (works as an interface)
	 * @param params A Params object that has been broadcasted to nodes in the cluster
	 * @return getId's method return (cell's id)
	 */
	public long getId(Params params) {
		return getId(x, y, z, params);
	}
	
	/**
	 * Overridden Object method equals(Object o)
	 */
	@Override
	public boolean equals(Object other) {
		boolean result;
		if ((other == null) || (getClass() != other.getClass())) {
			result = false;
		} else {
			Cell otherCell = (Cell) other;
			result = this.x == otherCell.x && this.y == otherCell.y && this.z == otherCell.z;
		}

		return result;
	}
	/**
	 * Overridden Object method hashCode()
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31).append(x).append(y).append(z).toHashCode();

	}
	
	/*
	 * Getters and Setters
	 */
	public long getX() {
		return x;
	}

	public void setX(long x) {
		this.x = x;
	}

	public long getY() {
		return y;
	}

	public void setY(long y) {
		this.y = y;
	}

	public long getZ() {
		return z;
	}

	public void setZ(long z) {
		this.z = z;
	}

	public int getPopulation() {
		return population;
	}

	public void setPopulation(int population) {
		this.population = population;
	}

}
